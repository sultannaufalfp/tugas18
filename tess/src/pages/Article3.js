import React from 'react';
import { Media } from 'reactstrap';

const Article3 = () => {
  return (
    <Media>
      <Media left href="#">
      </Media>
      <Media body>
        <Media heading>
        <h1>CLOUD COMPUTING</h1>
        <h6>Published by Sultan Naufal, 7 December 2020</h6>
        </Media>
        <h3>SEJARAH CLOUD COMPUTING</h3>
        <h5>Tahun 1960</h5>
        <p>John McCarthy, Pakar Komputasi dan kecerdasan buatan dari MIT. “Suatu hari nanti, komputasi akan menjadi Infrastruktur publik seperti halnya listrik dan telepon.” Ini adalah sebuah ide yang mengawali suatu bentuk komputasi yang kita kenal dengan istilah Komputasi awan.</p>
        <h5>Tahun 1995</h5>
        <p>Larry Ellison, pendiri perusahaan Oracle. “Network Computing” Ide ini sebenarnya cukup unik dan sedikit menyindir perusahaan Microsoft pada saat itu. Intinya, kita tidak harus "menanam" berbagai perangkat lunak kedalam PC pengguna, mulai dari sistem operasi hingga perangkat lunak lainya. Cukup dengan koneksi dengan server dimana akan disediakan sebuah environment yang mencakup berbagai kebutuhan PC pengguna.
           Pada era ini juga wacana “Network Computing” cukup populer. Banyak perusahaan yang menggalang sistem ini contohnya Sun Mycrosystem dan Novell Netware. Disayangkan kualitas jaringan komputer saat itu masih belum memadai, penggunapun cenderung memilih PC karena cenderung lebih cepat.</p>
        <h5>Akhir Era -90</h5>
        <p>Lahir konsep ASP (Application Service Provider) yang ditandai dengan kemunculan perusahaan pusat pengolahan data. Ini merupakan sebuah perkembangan pada kualitas jaringan komputer. Akses untuk pengguna menjadi lebih cepat.</p>
        <h5>Tahun 2000</h5>
        <p>Marc Benioff, mantan wakil presiden perusahaan Oracle. “salesforce.com” ini merupakan sebuah perangkat lunak CRM dengan basis SaaS (Software as a Service). Tak disangka gebrakan ini mendapat tanggapan hebat. Sebagai suksesor dari visi Larry Ellison, boss-nya. Dia memiliki sebuah misi yaitu “The End of Software”.</p>
        <h5>2005 - Sekarang</h5>
        <p>Cloud Computing sudah semakin meningkat popularitasnya, dari mulai penerapan sistem, pengunaan nama, dll. Amazon.com dengan EC2 (Elastic Computer Cloud); Google dengan Google App. Engine; IBM dengan Blue Cord Initiative; dsb. Perhelatan cloud computing meroket sebagaimana berjalanya waktu. Sekarang, sudah banyak sekali pemakaian sistem komputasi itu, ditambah lagi dengan sudah meningkatnya kualitas jaringan komputer dan beragamnya gadget yang ada. Contoh dari pengaplikasianya adalah Evernote, Dropbox, Google Drive, Sky Drive, Youtube, Scribd, dll.</p>
        <h3>PENGERTIAN CLOUD COMPUTING</h3>
        <p>Cloud Computing sebagai sebuah model yang memungkinkan adanya penggunaan sumberdaya (Resource) secara bersama-sama dan mudah, menyediakan jaringan akses dimana-mana , dapat di konfigurasi, dan layanan yang di gunakan sesuai keperluan (on Demand). Hal ini berarti layanan pada Cloud Computing dapat di sediakan dengan cepat dan meminimalisir interaksi dengan penyedia layanan (Vendor/Profider) Cloud Computing. 
            Sebagai sebuah teknologi perbaikan dari teknologi-teknologi sebelumnya (Grid Computing, Cluster Computing), Tentu terdapat SLA (Service Level Agreement) antara penyedia layanan berbasiS Cloud Computing dengan pengguna Akhir. SLA merupakan perjanjian mengikat yang harus di patuhi bersama antara penyedia layanan  dengan para pengguna layanan tersebut.</p>
        <h3>PENERAPAN CLOUD COMPUTING pada Perusahaan</h3>
        <p>Lintas Media Danawa (LMD), anak perusahaan Lintasarta, perusahaan ICT terkemuka di Indonesia saat ini, membawa teknologi cloud computing ke Indonesia.jadi perusahaan ini melayani on demand cloud computing dan private cloud computing di Indonesia.</p>
        <h6>Kelebihan</h6>
        <ul>
          <li>Data Terpusat</li>
          <li>Fleksibel</li>
          <li>Memiliki skalabilitas yang tinggi</li>
          <li>Investasi</li>
        </ul>
        <h6>Kekurangan</h6>
        <ul>
          <li>Kerahasiaan data tidak terjamin</li>
          <li>Sangat bergantung pada koneksi internet</li>
          <li>Tingkat keamanan yang belum terjamin</li>
        </ul>
       </Media>
    </Media>
  );
};

export default Article3;