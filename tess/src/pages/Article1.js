import React from 'react';
import { Media } from 'reactstrap';
import "./assets/Article.css";
const Article1 = () => {
  return (
    <div container>
      <Media>
      <Media left href="#">
      </Media>
      <Media body>
        <Media heading>
        <h1>Sejarah dan Macam-macam Komputasi Modern</h1>
        <h6>Published by Sultan Naufal, 7 December 2020</h6>
        </Media>
        <h2>Definisi Komputasi Modern.</h2>
        <p>Sebelum saya menjabarkan tentang sejarah komputasi modern, ada bainyak kalau saya jelaskan apa yang dimaksud komputasi.<br/>
          Komputasi adalah sebuah metode untuk menemukan pemecahan masalah menggunakan sebuah algoritma.<br/> 
          Sedangkan komputasi modern adalah komputasi yang dilakukan menggunakan perangkat komputer dalam proses pemecahan masalahnya.</p>
        <h3>Sejarah Komputasi Modern.</h3>
        <p>Sebenarnya sejarah komputasi sudah dimulai sejak terciptanya alat komputasi pertama “Sumreian Abacus” <br/>
          yang diduga diciptakan di Babylon tahun 2700-2300 sebelum masehi. <br/>
          Namun sejarah komputasi modern baru dimulai pada awal tahun 1900. 
          Untuk lebih jelasnya akan saya jabarkan kronologinya.</p>
          <ul>
            <li>
              1931 : sebuah paper karya C.E Wynn-Williams yang berjudul “The use of Thyratrons for High Speed Automatic Counting of Physical Phenomena” <br/> 
              membahas penggunaan Digital Electronics untuk proses komputasi.
            </li>
            <li>
                1934-1936 : Akira Nakashima merilis beberapa paper yang memperkenalkan switching circuit theory´ menggunakan Digital Electronics<br/>
                untuk operasi Boolean algebraic
            </li>
            <li>
            1937 : desain komputer buatan Atanasoff-Berry merupakan mesin “Digital Electronics” pertama walau tidak dapat di buat dalam bentuk program.
            </li>
            <li>
            1938 : Claude Shannon merilis paper yang berjudul “A Symbolic Analysis of Relay and Switching Circuits”
            </li>
            <li>
            1941 : penemu asal Jerman Konrad Zuse menciptakan Z3 computer yang merupakan mesin komputasi otomatis yang dapat di program pertama di dunia.
            </li>
            <li>
            1939-1945:pada saat perang dunia kedua, komputasi balistik dilakukan oleh wanita yang dipekerjakan sebagai “Computers” sekarang dikenal sebagai “Operators”.
            </li>
            <li>
            1946: ENIAC (Electronic Numerical Integrator and Computer) adalah komputer untuk penggunaan umum pertama di dunia. <br/> 
            Dapat diprogram ulang untuk menyelesaikan masalah komputasi secara menyeluruh, sudah turing complete, dan digital.
            </li>
            <li>
            1948 : komputer berisi program pertama Manchester Baby diciptakan di Victoria University of Manchester oleh Frederic C. Williams, Tom Kilburn dan Geoff Tootill, <br/> 
            dan menjalankan program pertamanya pada 21 juni  1948.
            </li>
            <li>
            1954-56:komputer transistor berprogram ETL Mark III, dikembangkan oleh Japan’s Electrotecchnical Laboratory.
            </li>
            <li>
            1954: 95 % penggunaan komputer digunakan untuk permasalahan teknik dan untuk penelitian.
            </li>
            <li>
            1968-1970: Diperkenalkannya microprocessor dengan intel 4004. Bermula dari “Busicom Project” sebagai desain Three-chip CPU buatan Masatoshi Shima tahun 1968.<br/> 
            Sebelum Tadashi Sasaki dari Sharp memahami desain single-chip CPU, yang ia diskusikan dengan Busicom dan intel pada tahun 1968. Intel 4004 di kembangkan sebagai single-chip microprocessor dari 1969 sampai 1970, dipimpin oleh Marcian Hoff dan Federico Faggin dari Intel dan Masatoshi Shima dari Busicom. Microprocessor menjadi pionir dari pengembangan microcomputers dan microcomputer revolution.
            </li>
            <li>
            1976: Texas Instruments memperkenalkan prosessor mereka TMS9900.
            </li>
            <li>
            1983 : komputer Apple “Lisa” komputer pertama dengan GUI diproduksi.
            </li>
            <li>
            1990: machintos portable dari Apple dan Touchstone Delta supercomputer dari intel diproduksi.
            </li>
          </ul>
          <h3>Macam-macam Komputasi Modern</h3>
          <h5>Mobile computing</h5>
          <p>Mobile Computing merupakan paradigma dari teknologi yang mampu melakukan komunikasi walaupun user melakukan perpindahan. <br/>
          Dengan ini proses komputasi tidak perlu dilakukan di satu tempat saja namun bisa dilakukan dimana-mana, dikarenakan mobile computing tidak memerlukan koneksi dengan kabel.</p>
          <h5>Grid computing</h5>
          <p>Grid computing  merupakan sebuah sistem pemecahan masalah komputasi skala besar, <br/> 
          yang melibatkan banyak komputer yang terhubung dan terdistribusi untuk memecahkan masalah tersebut.</p>
          <h5>Cloud computing</h5>
          <p>Cloud computing merupakan pemanfaatan dari teknologi komputasi dengan internet, <br/> 
          ia merupakan suatu metode komputasi yang disajikan sebagai suatu layanan yang dapat diakses melalui internet, guna untuk mencari informasi yang tersimpan di dalam server internet, dan untuk menyimpan suatu data yang dapat digunakan dilain hari.</p>
      </Media>
    </Media>
    </div>
  );
};

export default Article1;