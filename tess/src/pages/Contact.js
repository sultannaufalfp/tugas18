import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input,
Card, CardTitle, CardText} from 'reactstrap';

const Contact = (props) => {
  return (
    <Form>
        <Card body>
          <CardTitle tag="h5">Contact Us</CardTitle>
          <CardText>If there something you want to ask, you can contact us by using the form below.</CardText>
        </Card>
      <Row form>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleEmail">Email</Label>
            <Input type="email" name="email" id="exampleEmail" placeholder="Your Email" />
          </FormGroup>
        </Col>
        <Col md={6}>
        <FormGroup>
        <Label for="exampleText">Question/Feedback</Label>
        <Input type="textarea" name="text" id="exampleText" placeholder="Your question/Feedback"/>
      </FormGroup>
        </Col>
      </Row>
      <Button>Submit</Button>
    </Form>
  );
}

export default Contact;