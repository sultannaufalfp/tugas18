import React from 'react';
import { Jumbotron, Button } from 'reactstrap';

const Home = (props) => {
  return (
    <div>
      <Jumbotron>
        <h1 className="display-3">Hello, world!</h1>
        <p className="lead">This is a mini site created using react.js and reactstrap</p>
        <hr className="my-2" />
        <p>Welcome, and have a good time!!!!!</p>
        <p className="lead">
          <Button color="primary" href="/about/">Learn More</Button>
        </p>
      </Jumbotron>
    </div>
  );
};

export default Home;