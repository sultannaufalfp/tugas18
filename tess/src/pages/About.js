import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import img from './assets/Foto.jpg';
import "./assets/About.css";
import {
    Card, CardImg, CardTitle, CardText, CardDeck, CardBody
  } from 'reactstrap';

const About = (props) => {
  return (
    <CardDeck>
    <Card>
      <CardImg className="photo" src={img} alt="Card image cap" />
      <CardBody>
        <CardTitle tag="h5">Biodata</CardTitle>
        <CardText>
            Name: Sultan Naufal Fardhan Prima
            <br/>
            DOB: 22 January 1998
            <br/>
            Age: 22
            <br/>
            Birthplace: Purwokerto
            <br/>
            Address: Bekasi, West Java, Indonesia
            <br/>
            Occupation: Student at Glint's Academy #9 FE Class
        </CardText>
      </CardBody>
    </Card>
    <Card>
      <CardBody>
        <CardTitle tag="h5">Summary</CardTitle>
        <CardText>
        Hello everyone.<br/> My name is Sultan Naufal Fardhan Prima, but you can call me Sultan.<br/>I'm a Fresh Graduate from Gunadarma University majoring in Computer Science.<br/>
          I'm able to use HTML, CSS, Javascript, and Bootstraps at a begginer level. Currently i joined Glints Academy Batch 9 Front End Class to expand my knowledge and to re-learn all that i miss during my college years.
        </CardText>
      </CardBody>
    </Card>
  </CardDeck>
);
};

export default About;