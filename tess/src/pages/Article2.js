import React from 'react';
import { Media } from 'reactstrap';

const Article2 = () => {
  return (
    <Media>
      <Media left href="#">
      </Media>
      <Media body>
        <Media heading>
        <h1>Tentang Komputer dan Internet</h1>
        <h6>Published by Sultan Naufal, 7 December 2020</h6>
        </Media>
        <h2>KOMPUTER dan INTERNET</h2>
        <h3>Apa itu komputer?</h3>
        <p>Komputer adalah alat yang dipakai untuk mengolah data menurut prosedur yang telah dirumuskan.
          Kata computer secara umum pernah dipergunakan untuk mendefiniskan orang yang melakukan perhitungan 
          aritmatika, dengan atau tanpa mesin pembantu.</p>
        <p>Menurut Barnhart Concise Dictionary of Etymology, kata tersebut digunakan dalam bahasa Inggris 
          pada tahun 1646 sebagai kata untuk “orang yang menghitung” kemudian menjelang 1897 juga digunakan sebagai “alat hitung mekanis”. Selama Perang Dunia II kata tersebut menunjuk kepada para pekerja wanita Amerika Serikat dan Inggris yang pekerjaannya menghitung jalan artileri perang dengan mesin hitung.</p>
        <h4>Komputer terdiri atas 2 bagian besar, yaitu perangkat lunak (software) dan perangkat keras (hardware).</h4>
        <h5>Perangkat Keras (Hardware)</h5>
        <ul>
          <li>Pemroses atau CPU : sebagai unit pengolah data</li>
          <li>Memori RAM : tempat penyimpanan data sementara</li>
          <li>Hard drive : media penyimpanan data semi permanen</li>
          <li>Perangkat masukan, : media yang digunakan untuk memasukkan data untuk diproses oleh CPU, seperti mouse, keyboard, dan tablet</li>
          <li>Perangkat keluaran : media yang digunakan untuk menampilkan hasil keluaran pemrosesan CPU, seperti monitor, speaker, headset, plotter, proyektor, dan printer.</li>
        </ul>
        <h5>Perangkat Lunak (Software)</h5>
        <ul>
          <li>Sistem operasi : Program dasar pada komputer yang menghubungkan pengguna dengan hardware komputer. Sistem operasi yang biasa digunakan adalah Linux, Windows, dan Mac OS. Tugas sistem operasi termasuk (namun tidak hanya) mengatur eksekusi program di atasnya, koordinasi input, output, pemrosesan, memori, serta instalasi software.</li>
          <li>Program komputer : Merupakan aplikasi tambahan yang dipasang sesuai dengan sistem operasinya</li>
        </ul>
        <h5>Komputer memiliki beberapa jenis, yaitu:</h5>
        <ul>
          <li>Komputer analog : mesin komputer yang diciptakan untuk mengolah data yang bersifat kuantitatif dalam bentuk angka, huruf, tanda baca dan lain-lain. Yang pemrosesnya dilaksanakan berdasarkan teknologi yang mengubah sinyal menjadi kombinasi bilangan 0 dan 1.</li>
          <li>Mikrokomputer : adalah sebuah kelas komputer yang menggunakan mikroprosesor sebagai CPU utamanya. Komputer mikro juga dikenal sebagaiPersonal Computer (PC), Home Computer, atau Small-business Computer. Komputer mikro yang diletakkan di atas meja kerja dinamakan dengan desktop, sedangkan yang dapat dijinjing (portabel) dinamakan dengan Laptop.</li>
          <li>Mini komputer : adalah kelas komputer multi-user yang dalam spektrum komputasi berada di posisi menengah di bawah kelas komputer mainframe dan sistem komputer single-user seperti komputer pribadi.</li>
          <li> Mainframe komputer : adalah komputer besar yang digunakan untuk memproses data dan aplikasi yang besar. Mainframe pada umumnya digunakan dalam Perusahaan atau Organisasi yang menangani data seperti sensus, riset penelitian, keperluan militer atau transaksi finansial. Mainframe dapat melayani ratusan pengguna pada waktu yang bersamaan.</li>
          <li>Super computer : digunakan untuk tugas penghitungan-intensif seperti prakiraan cuaca, riset iklim (termasuk riset pemanasan global, pemodelan molekul, simulasi fisik (seperti simulasi kapal terbang dalam terowongan angin, simulasi peledakan senjata nuklir, dan riset fusi nuklir), analisikrip, dan lain-lain. Militer dan agensi sains salah satu pengguna utama superkomputer.</li>
        </ul>
        <h3>INTERNET</h3>
        <p>Internet berasal dari kata interconnection-networking, yang merupakan sistem global dari seluruh jaringan komputer yang saling terhubung menggunakan standar Internet Protocol Suite (TCP/IP) sebagai protokol pertukaran paket (packet switching communication protocol).</p>
        <h4>Pengertian INTERNET </h4>
        <h5>pengertian internet menurut segi ilmu pengetahuan</h5>
        <p>Sebuah perpustakaan besar yang didalamnya terdapat jutaan (bahkan milyaran) informasi atau data yang dapat berupa teks, grafik, audio maupun animasi dan lain lain dalam bentuk media elektronik. Semua orang bisa berkunjung ke perpustakaan tersebut kapan saja serta dari mana saja.</p>
        <h5>pengertian internet menurut segi komunikasi</h5>
        <p>Sarana yang sangat efektif dan efesien untuk melakukan pertukaran informasi jarak jauh maupun jarak dekat, seperti di dalam lingkungan perkantoran, tempat pendidikan, atapun instansi terkait.</p>
        <p>Bertambahnya jumlah pengguna akses internet tersebut memang sangat wajar sekali, saat ini internet bukan hanya digunakan sebagai sarana komunikasi atau pun sarana mencari informasi saja, tetapi juga telah digunakan sebagai sarana untuk mencari uang. Harga tarif akses internet pun saat ini juga telah lebih murah jika dibandingkan dengan beberapa tahun yang lalu. dan pengguna akses internet pun bukan hanya orang yang berada di wilayah perkotaan saja, orang yang tinggal di pedesaan pun juga dapat mengakses internet.</p>
      </Media>
    </Media>
  );
};

export default Article2;