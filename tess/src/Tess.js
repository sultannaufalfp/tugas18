import Home from "./pages/Home"
import About from "./pages/About"
import Contact from "./pages/Contact"
import Article1 from "./pages/Article1"
import Article2 from "./pages/Article2"
import Article3 from "./pages/Article3"
import { Switch, Route, Link } from "react-router-dom";
import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

const Tess = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/home/">MySite</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/home/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/about/">About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/contact/">Contact</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Article
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                <Link to="/article1">Article1</Link>
                </DropdownItem>
                <DropdownItem>
                <Link to="/article2">Article2</Link>
                </DropdownItem>
                <DropdownItem>
                <Link to="/article3">Article3</Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar>
      <Switch>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/article1">
          <Article1 />
        </Route>
        <Route path="/article2">
          <Article2 />
        </Route>
        <Route path="/article3">
          <Article3 />
        </Route>
      </Switch>
    </div>
  );
}

export default Tess;